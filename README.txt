CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------
POWr Social Media Icons is a mobile-responsive, fully-customizable plugin that
you can edit right in the page.
Add the widget to your site, or use the token (also called shortcode)
[powr-social-media-icons label="Enter a Label"] to add Social Media Icons to any
text within any content.

Don't settle for bland, cookie-cutter social links! 

Engage your users will with custom social media icons from POWr.  Link to any
account on Facebook, Flickr, Foursquare, Github, Google+, Instagram, Linkedin,
Pinterest, Tumblr, Twitter, YouTube, and dozens more.  The best way for users to
explore your social sphere.

Fully-customizable on any site with adjustable animations, colors, borders and
backgrounds.  Edit right in your page with no coding or programming required. 
Best of all, mobile-responsive design means your feed will look great on any
phone, tablet, or computer.


The basic version of the POWr Social Media Icons is free!

POWr plugins can be used an virtually any website, learn more at http://powr.io

REQUIREMENTS
------------
There are no requirements for this module. If you'd like to use the Social
Media Icons plugin as a block, the the block module is required.

INSTALLATION
------------
Install as usual, see
http://www.powr.io/tutorials/how-to-add-social-media-icons-plugin-to-your-drupal-site
for further information.
 Briefly:
  * Download the .zip file
* Within your Drupal admin, go to Modules->Install New Module, and locate the
.zip file
  * Within the modules page, enable the POWr Social Media Icons Module.
Note: you may need to clear your caches after installing:
Configuration->Development->Clear All Caches.
* To use as a block: visit Structure->blocks and drag the POWr Social Media
Icons to your desired block.
* To use within ANY post, article, or other content: simply write the
token/shortcode in any text: [powr-social-media-icons label="Enter a Label"].
You also have instant access to all other POWr plugins, full list at
https://www.powr.io
* View your site and your Social Media Icons will appear right in the page.
Click the gears icon and customize right in the page.

CONFIGURATION
-------------
Configure Social Media Icons right in the page! Just visit your site, click the
gears icon, and the POWr Editor will open.

TROUBLESHOOTING
---------------
 * If the plugin does not appear in your page
  - Is the module enabled?
  - Clear the caches: Configuration->Performance->Clear all caches
 * Need help?
  - Contact: support@powr.io

FAQ
---
 Q: How can I add Social Media Icons to a blog post or page?
A: Just add the token/shortcode [powr-Social Media Icons label='Enter a Label']
(including the brackets)
in your post where you'd like your Social Media Icons to appear. Then visit
your page!

 Q: How can I add  Social Media Icons to my site multiple times?
 A: Just be sure to specify different label text.
For example: [powr-social-media-icons label='MY LABEL 1'],
[powr-social-media-icons label='MY LABEL 2'] for each time you use the plugin

 Q: Why do you require me to sign up?
A: Signing up is what allows us to track which plugins are yours, and allows
you to edit them right in your page.

 Q: Will my users see the settings icon in the corner of the Social Media Icons?
 A: No! You only see the settings icon because you own the plugins.
    To temporarily hide it, click on your page and then type 'p + down arrow'
(the letter p followed by the down arrow). You can show the icon again by
typing 'p + up-arrow'.

 Q: I can't see the settings icon anymore, what happened?
A: You are just logged out of POWr.io. You can show the icon again by typing 'p
+ up-arrow'.

 Q: Is POWr Social Media Icons Free?
 A: Yes, the standard version of Social Media Icons is completely free!

 Q: Why should I upgrade?
A: Upgrading removes all POWr watermarks, speeds up widget loading speed, gets
you premium support, and most importantly supports the creation of more great
plugins!

MAINTAINERS
-----------
Current maintainers:
 * POWr.io - https://drupal.org/user/2814991
   Contact: info@powr.io
